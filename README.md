# My custom AWX EE

I made this repo because I didnt find any custom EE image which has the community.general, to make a dynamic proxmox inventory, installed.



## Requirements

- Podman
- Python

## Usage

1. Activate VENV
```bash
python3 -m venv builder
source builder/bin/activate
pip install -r requirements-venv.txt
```

2. Login to the registry
```bash
podman login registry.gitlab.com
``` 

3. Build the container image
```bash
ansible-builder build -t registry.gitlab.com/jokulab/awx-ee-custom:<tag>
```

4. Push the container image to the registry
```bash
podman push registry.gitlab.com/jokulab/awx-ee-custom:<tag>
```
